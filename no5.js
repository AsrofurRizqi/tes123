//1
function angka(angka){
    if(typeof(angka) == "number"){
        let hasil= [];
        let a = ""+angka;
        let b = a.split("0");
        b.map(function(a){
            let x = a.split("");
            hasil.push(x.sort().join(""));
        })
        console.log(hasil.join(""));
    }else{
        console.log("hanya nomer")
    }
}
angka(56431046145403146);

//2
function huruf(huruf){
    let a = huruf.split("")
    let b = a.sort();
    let c = b.filter((v,i) => a.indexOf(v) == i)
    console.log(c.join(""));
}
huruf("pluginsangatkerensekali");

//3
function urutan(n,a,b){
    let hasil = [a];
    for(let x=0;x<n;x++){
        let a = hasil[x] +=b
        hasil.push(a);
    }
    console.log(hasil)
}
urutan(3,1,5)

//4
function balik(a) {
    let arr = [''];
    let hasil = [];
    let j = 0;

    for (let i = 0; i < a.length; i++) {
        if (a.charAt(i) == " ") {
            j++;
            arr.push('');
        } else {
            arr[j] += a.charAt(i);
        }
    }
    for(let x = arr.length - 1;x>=0;x--){
        hasil.push(arr[x]);
    }
    console.log(hasil.join(" "));
}
balik("Saya cinta PLUGIN");

//5
function iq(nomer){
    let b = nomer.split("")
    let filter = b.filter(function(item, pos){
        return b.indexOf(item)== pos; 
      });
    let c = b.reduce((r, n, i) => {
        n === filter[1] && r.push("index ke-"+(i+1));
        
        return r;
      }, []);
    console.log(c);
}
iq("11111111111911111111119111111111111111191111");

//6
//database digunakan untuk menyimpan dan mengolah data agar dapat menghasilkan info yg berguna.
//contoh query
//SELECT * FROM table;
//DELETE FROM table WHERE x=x;
//INSERT INTO table VALUE(1,2,3,4);
//UPDATE FROM table SET x=x y=y WHERE z=z;
//CREATE DATABASE nama;

//framework merupakan kumpulan kerangka dari suatu program untuk tujuan tertentu.
//perbedaan framework dan library yaitu framework lebih terarah dan memiliki alur kerja tersendiri sesuai pembuatnya.
//kalau library juga kumpulan kode yang disusun oleh developernya namun dapat digunakan atau alur sesuai dengan keinginan masing-masing

//framework js: react,express,angular,node,vue,ember,backbone

//frontend berhubungan dengan tampilan interface dan gui, merupakan penghubung antara pengguna dengan sistem atau backend..forntend sangat dibutuhkan oleh backend agar dapat menerima dan menampilkan informasi ke user.
//backend merupakan server yang berguna untuk melakukan proses yang diterima dari frontend, dan juga menyimpan data,mengolah


//7
//built in function js
//length , panjang string atau element
let q = "abcd"
console.log(q.length);

//indexof , mencari index element pada string,array
let w = "abcdefg"
console.log(w.indexOf("g"));

//charat, return char pada index tertentu
let e = "abcdefgh"
console.log(e.charAt(2));

//tostring, number to string
let r = 12345;
console.log(r.toString());

//concat, gabung string
let t = "abc";
let y = "def";
console.log(t.concat(y));

//split string atau array
let u = "abcd0efgh"
console.log(u.split("0"));

//map, perulangan pada elemen array / string
let i = [1,2,3,4,5];
console.log(i.map(a=> a*2))