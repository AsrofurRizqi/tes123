//promes1
let a = true;
const promes = new Promise((resolve, reject) =>{
    if(a) {
        resolve('fullfilled')
    }else{
        reject('reject')
    }
})
console.log(promes);

//promes2
var promes2 = new Promise(function(resolve, reject) {
    setTimeout(function() {
        resolve('tes');
    }, 2000);
});

promes.then(function(data) {
    console.log(data);
});

//promes3
testing
    .finally(() => console.log('selesai menunggu'))
    .then(response => console.log('ok : '+response))
    .catch(response => console.log('not ok: '+ response))
console.log('selesai')


//fetch
function data(){
    fetch('https://dhiyo-api-article.herokuapp.com/articles')
        .then((response)=> response.json())
        .then((data)=>{
            for(let x=0;x<data.data.length;x++){
                console.log("title "+x+" :"+data.data[x].tittle)
            }
        })
        .catch((err) => console.log(err));
}
data();