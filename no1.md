1. variabel dan tipe data js
```Variabel adalah sebuah nama yang mewakili sebuah nilai. Variabel bisa diisi dengan berbagai macam nilai seperti string (teks), number (angka), objek, array, dan sebagainya.```

- string tipe data karakter atau teks
- number tipe data bilangan
- object kumpulan data yg ditandai {} dan memiliki tipe data yg beraneka ragam seperti array di dalam objek
- array kumpulan data yg ditandai [] dan memiliki tipe data yg sama

2. perbedaan if-else dan switch case terdapat dalam pengodingannya `else` untuk nilai default dan pada switch memakai nilai `default`, switch case lebih cocok untuk percabangan yg lebih banyak karena lebih simpel.

if(5 > 6){
    console.log("tes")
}else{
    console.log("tes2")
}

switch(isi) {
    case 1 :
    console.log("tes1");
    break;
    case 2 :
    console.log("tes2");
    break;
}

3. fungsi merupakan rangkain beberapa kode yang di deklarasikan melalui variabel fungsi atau juga dapat bersifat anonimus contoh fungsi berupa fungsi deklarasi dan fungsi arrow, method built in function yang dapat berhubungan dengan objek atau array.

function tes(){
    console.log("tes1");
}
tes();

let a = [a,b,c,d];
console.log(a.join());

4. sychronus merupakan eksekusi perintah pada javascript secara berurutan dari atas sampai bawah, jika ada yg error maka proses terhenti, asynchronus merupakan eksekusi perintah pada javascript yang tidak singkron seperti terdapat perintah yg delay atau menunggu seperti pada api tetapi program tidak terhenti.
